from flask import Flask, render_template, request, jsonify
import requests
import os 

app = Flask(__name__)

# key set as global variable for security purposes
API_KEY = os.getenv(ALPHA_VANTAGE_API_KEY)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/stock-info', methods=['POST'])
def stock_info():
    stock_symbol = request.form.get('stockSymbol')
    if not stock_symbol:
        return "Stock symbol is required", 400

    overview = fetch_company_overview(stock_symbol)
    quote = fetch_global_quote(stock_symbol)
    news = fetch_recent_news(stock_symbol)  

    data = {
        "overview": overview,
        "quote": quote,
        "news": news
    }

    return render_template('stock_info.html', data=data)

def fetch_company_overview(symbol):
    url = f'https://www.alphavantage.co/query?function=OVERVIEW&symbol={symbol}&apikey={API_KEY}'
    response = requests.get(url)
    return response.json()

def fetch_global_quote(symbol):
    url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey={API_KEY}'
    response = requests.get(url)
    return response.json().get("Global Quote", {})

def fetch_recent_news(symbol):
    url = f'https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers={symbol}&apikey={API_KEY}&limit=10'
    response = requests.get(url)
    data = response.json()
    feed = data.get("feed", [])
    news_items = []
    for article in feed[:5]:
        news_item = {
            'title': article['title'],
            'url': article['url'],
            'summary': article['summary']
        }
        news_items.append(news_item)
    return news_items


if __name__ == '__main__':
    app.run(debug=True)
