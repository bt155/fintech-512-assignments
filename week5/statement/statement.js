function statement(invoice, plays) {
  let totalAmount = 0;
  let volumeCredits = 0;
  let result = `Statement for ${invoice.customer}\n`;
  const format = formatUSD; // Edited: Extracted currency formatting logic to a function
  
  for (let perf of invoice.performances) {
      const play = plays[perf.playID];
      let thisAmount = calculateAmount(perf, play); // Edited: Extracted amount calculation to a function
      
      // add volume credits
      volumeCredits += calculateVolumeCredits(perf, play); // Edited: Extracted volume credits calculation to a function
      
      // print line for this order
      result += `  ${play.name}: ${format(thisAmount / 100)} (${perf.audience} seats)\n`;
      totalAmount += thisAmount;
  }
  result += `Amount owed is ${format(totalAmount / 100)}\n`;
  result += `You earned ${volumeCredits} credits\n`;
  return result;
}

// Edited: New function for currency formatting
function formatUSD(amount) {
  return new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", minimumFractionDigits: 2 }).format(amount);
}

// Edited: New function for calculating amount
function calculateAmount(perf, play) {
  let thisAmount = 0;
  switch (play.type) {
      case "tragedy":
          thisAmount = 40000;
          if (perf.audience > 30) {
              thisAmount += 1000 * (perf.audience - 30);
          }
          break;
      case "comedy":
          thisAmount = 30000;
          if (perf.audience > 20) {
              thisAmount += 10000 + 500 * (perf.audience - 20);
              thisAmount += 300 * perf.audience;
          }
          break;
      default:
          throw new Error(`unknown type: ${play.type}`);
  }
  return thisAmount;
}

// Edited: New function for calculating volume credits
function calculateVolumeCredits(perf, play) {
  let volumeCredits = Math.max(perf.audience - 30, 0);
  if ("comedy" === play.type) volumeCredits += Math.floor(perf.audience / 5);
  return volumeCredits;
}
