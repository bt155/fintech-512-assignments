Flask employes the Model-View-Controller (MVC) pattern as the following:
1._**Models**('User', 'Post'): These two represent the applications's data and flow logic. They manage user informaiton and blog posts
2._**Templates**: This act as the View component, rendering model data into user and blog post information
3._**Views/Controllers**('AuthViews', 'BlogViews'): Sers as Controllers, processing user input, updating models, selecting views for rendering. They handle authentication and blog post management.
4._**FlaskApp and Blueprint**: These two organize the application and its modular sections, such as auth and blog mangament 
5._**Database**: The database is crucial as it interfaces with modesl for data persistence, storing and retreving application data
