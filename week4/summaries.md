# Your paper and video summaries go here.

# Rubric:
* Write a short summary of the paper/video, as follows:
* First line: Paper Title, citation source
* Keywords: List keywords from paper, or select 4-8 most relevant terms from the paper
* First paragraph: describe the main points of the paper/video.
* Second paragraph: Present the paper/video's strengths and weaknesses from your point of view.

__ UML Quick Reference:__
Allen talks about OMG UML version 2.5 standard and he also provides a comprehensive overview of various UML diagrams and elements, ensuring that readers have a solid grasp of UML’s capabilities for modeling software design.
1.	Basic Concepts – introduces UML basics, including the importance of comments and organizational diagrams such as packages and subsystems, highlighting how they group functionally similar classes and runtime objects, respectively.
2.	Use-Case Diagrams – Specify participants in a use case and the relationships between use cases. 
3.	Activity and State Diagrams – Details the notation for illustrating the flow of activities or states within a system, including starting, stopping and decision-making processes.
4.	Class diagrams and patterns – explain the structure of various calss diagrams and associations, inheritance etc… 
5.  Interaction Diagrams: Covers sequence and collaboration/communication diagrams, focusing on how objects interact through messages over time or within a specific context.
