class Money:
    def __init__(self, amount):
        self._amount = amount

    def __eq__(self, other):
        if not isinstance(other, Money):
            return False
        return self._amount == other._amount and self.__class__ == other.__class__

    @classmethod
    def dollar(cls, amount):
        return Dollar(amount)

    @classmethod
    def franc(cls, amount):
        return Franc(amount)

    def times(self, multiplier):
        raise NotImplementedError("Subclasses must implement this method")


class Dollar(Money):
    def times(self, multiplier):
        return Money.dollar(self._amount * multiplier)


class Franc(Money):
    def times(self, multiplier):
        return Money.franc(self._amount * multiplier)


def test_money_equality():
    assert Money.dollar(5) == Money.dollar(5)
    assert Money.dollar(5) != Money.dollar(6)
    assert Money.franc(5) == Money.franc(5)
    assert Money.franc(5) != Money.franc(6)
    assert Money.franc(5) != Money.dollar(5)


def test_dollar_multiplication():
    five = Money.dollar(5)
    assert five.times(2) == Money.dollar(10)
    assert five.times(3) == Money.dollar(15)


def test_franc_multiplication():
    five = Money.franc(5)
    assert five.times(2) == Money.franc(10)
    assert five.times(3) == Money.franc(15)


test_money_equality()
test_dollar_multiplication()
test_franc_multiplication()
