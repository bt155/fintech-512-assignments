template <typename E>
class Queue
{
public:
    void Enqueue(const E &element)
    { /* ... */
    }
    E *Dequeue()
    { /* Returns NULL if empty ... */
    }
    size_t size() const
    { /* ... */
    }
};
