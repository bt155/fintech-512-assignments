#include <gtest/gtest.h>
#include "queue.h"

template <typename E>
class QueueTest : public testing::Test {
protected:
    Queue<E> q0_;
    Queue<E> q1_;
    Queue<E> q2_;

    void SetUp() override {
        q1_.Enqueue(1);
        q2_.Enqueue(2);
        q2_.Enqueue(3);
    }
};

using IntQueueTest = QueueTest<int>;

TEST_F(IntQueueTest, IsEmptyInitially) {
    EXPECT_EQ(0, q0_.size());
}

TEST_F(IntQueueTest, DequeueWorks) {
    EXPECT_THROW(q0_.Dequeue(), std::out_of_range);

    EXPECT_EQ(1, q1_.Dequeue());
    EXPECT_EQ(0, q1_.size());  

    EXPECT_EQ(2, q2_.Dequeue());
    EXPECT_EQ(1, q2_.size());  
    EXPECT_EQ(3, q2_.Dequeue());
    EXPECT_EQ(0, q2_.size());  
}

