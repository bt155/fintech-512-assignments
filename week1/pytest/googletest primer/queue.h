#ifndef QUEUE_H
#define QUEUE_H

#include <vector>
#include <stdexcept>

template <typename E>
class Queue
{
public:
    Queue() {}

    void Enqueue(const E &element)
    {
        elements.push_back(element);
    }

    E Dequeue()
    {
        if (IsEmpty())
        {
            throw std::out_of_range("Queue is empty");
        }
        E frontElement = elements.front();
        elements.erase(elements.begin());
        return frontElement;
    }

    size_t size() const
    {
        return elements.size();
    }

    bool IsEmpty() const
    {
        return elements.empty();
    }

private:
    std::vector<E> elements;
};

#endif // QUEUE_H
