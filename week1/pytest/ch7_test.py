class Money:
    def __init__(self, amount):
        self._amount = amount

    def __eq__(self, other):
        if not isinstance(other, Money):
            return False
        return self._amount == other._amount and self.__class__ == other.__class__


class Dollar(Money):
    def times(self, multiplier):
        return Dollar(self._amount * multiplier)


class Franc(Money):
    def times(self, multiplier):
        return Franc(self._amount * multiplier)


def test_money_equality():
    assert Dollar(5) == Dollar(5)
    assert Dollar(5) != Dollar(6)
    assert Franc(5) == Franc(5)
    assert Franc(5) != Franc(6)
    assert Franc(5) != Dollar(5)


def test_dollar_multiplication():
    five = Dollar(5)
    assert five.times(2) == Dollar(10)
    assert five.times(3) == Dollar(15)


def test_franc_multiplication():
    five = Franc(5)
    assert five.times(2) == Franc(10)
    assert five.times(3) == Franc(15)


test_money_equality()
test_dollar_multiplication()
test_franc_multiplication()
