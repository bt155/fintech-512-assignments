import pytest

from square import Square


def test_area():
    square = Square(10)
    area = square.area()
    assert area == 100


def test_length_with_wrong_type():
    with pytest.raises(TypeError):
        square = Square('10')


def test_length_with_zero_or_negative():
    with pytest.raises(ValueError):
        square = Square(0)
        square = Square(-1)
