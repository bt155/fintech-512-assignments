class Dollar:
    def __init__(self, amount):
        self._amount = amount

    def times(self, multiplier):
        return Dollar(self._amount * multiplier)


def test_multiplication():
    five = Dollar(5)
    product = five.times(2)
    assert product == Dollar(10)
    product = five.times(3)
    assert product == Dollar(15)


test_multiplication()
