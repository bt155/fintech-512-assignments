class Franc:
    def __init__(self, amount):
        self._amount = amount

    def times(self, multiplier):
        return Franc(self._amount * multiplier)

    def __eq__(self, other):
        if not isinstance(other, Franc):
            return False
        return self._amount == other._amount


def test_franc_multiplication():
    five = Franc(5)
    assert five.times(2) == Franc(10), "5 CHF * 2 should be 10 CHF"
    assert five.times(3) == Franc(15), "5 CHF * 3 should be 15 CHF"


def test_franc_equality():
    assert Franc(5) == Franc(5), "Francs with the same amount should be equal"
    assert Franc(5) != Franc(
        6), "Francs with different amounts should not be equal"


# Running the tests
test_franc_multiplication()
test_franc_equality()
