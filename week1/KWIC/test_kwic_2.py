import unittest
from kwic import generate_kwic_index


class TestKWIC(unittest.TestCase):

    def test_kwic(self):
        ignore_words = ['is', 'the', 'of', 'and', 'as', 'a', 'but']
        titles = [
            "Descent of Man",
            "The Ascent of Man",
            "The Old Man and The Sea",
            "A Portrait of The Artist As a Young Man",
            "A Man is a Man but Bubblesort IS A DOG"
        ]
        expected_output = [
            "a portrait of the ARTIST as a young man",
            "the ASCENT of man",
            "a man is a man but BUBBLESORT is a dog",
            "DESCENT of man",
            "a man is a man but bubblesort is a DOG",
            "descent of MAN",
            "the ascent of MAN",
            "the old MAN and the sea",
            "a portrait of the artist as a young MAN",
            "a MAN is a man but bubblesort is a dog",
            "a man is a MAN but bubblesort is a dog",
            "the OLD man and the sea",
            "a PORTRAIT of the artist as a young man",
            "the old man and the SEA",
            "a portrait of the artist as a YOUNG man"
        ]
        self.assertEqual(generate_kwic_index(
            titles, ignore_words), expected_output)


if __name__ == '__main__':
    unittest.main()
