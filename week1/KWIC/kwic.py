import sys
from io import StringIO


def generate_kwic_index(titles, ignore_words):
    ignore_words_set = set(word.lower() for word in ignore_words)
    kwic_entries = []

    for title in titles:
        words = title.split()
        for i, word in enumerate(words):
            lower_word = word.lower()
            if lower_word not in ignore_words_set:

                formatted_words = [w.lower() if j != i else w.upper()
                                   for j, w in enumerate(words)]
                formatted_title = ' '.join(formatted_words)
                kwic_entries.append(formatted_title)

    sorted_kwic_entries = sorted(
        kwic_entries,
        key=lambda x: next((word for word in x.split() if word.isupper()), x)
    )

    return sorted_kwic_entries


def simulate_input(ignore_words_str, titles_str):
    input_data = ignore_words_str + '\n' + titles_str
    sys.stdin = StringIO(input_data)


def main():
    try:
        ignore_words_input = input()
    except EOFError:
        ignore_words = []
    else:
        ignore_words = ignore_words_input.split(',')

    titles = []
    while True:
        try:
            title = input()
            if title:
                titles.append(title)
            else:
                break
        except EOFError:
            break

    kwic_index = generate_kwic_index(titles, ignore_words)
    for title in kwic_index:
        print(title)


if __name__ == '__main__':
    main()
