# Your paper and video summaries go here.

# Rubric:
* Write a short summary of the paper/video, as follows:
* First line: Paper Title, citation source
* Keywords: List keywords from paper, or select 4-8 most relevant terms from the paper
* First paragraph: describe the main points of the paper/video.
* Second paragraph: Present the paper/video's strengths and weaknesses from your point of view.

PSP Summary:
The Personal Software Process(PSP) gives a detail structured approach for developing module-level programs. It includes multiple steps like panning, with clear problem descripitons. And a conceptual designs with time estimates for each components are developed. The development pase involves implementing the design and recording development time. Acceptance testing includes testing the program, fixing defects, as well as recording the testing time. Evaluation is the total time spent and noting any issues or opportunities a thoroughly tested program and closed planning, development, and evaluation issues.

Chinook.db:
1. There are **59** customers. We use cur.execute("SELECT COUNT(*) FROM customer;") to find the number of customers

2. The name of the IT employees are: **Robert King & Laura Callahan**.  We use cur.execute("SELECT FirstName, LastName FROM employees WHERE Title LIKE '%IT Staff%';") to find the names

3. The most popular genre is **Rock**. '\n'
query = SELECT g.Name AS Genre, COUNT(ii.TrackId) AS TrackSold
FROM genres g
JOIN tracks t ON g.GenreId = t.GenreId
JOIN invoice_items ii ON t.TrackId = ii.TrackId 
GROUP BY g.Name
ORDER BY TrackSold DESC
LIMIT 1;

4. The least popular genra is **Rock and Roll**
query = SELECT g.Name AS Genre, COUNT(ii.TrackId) AS TrackSold
FROM genres g
JOIN tracks t ON g.GenreId = t.GenreId
JOIN invoice_items ii ON t.TrackId = ii.TrackId 
GROUP BY g.Name
ORDER BY TrackSold
LIMIT 1;

5. The most popular artist is **Iron Maiden**
query = SELECT ar.Name AS Artist, COUNT(ii.TrackId) AS TrackSold
FROM artists ar
JOIN albums al ON ar.ArtistId = al.ArtistId
JOIN tracks t ON al.AlbumId = t.AlbumId 
JOIN invoice_items ii ON t.TrackId = ii.TrackId
GROUP BY ar.Name
ORDER BY TrackSold DESC
LIMIT 1;

6. The number of albums by 'Miles Davis' in the database is **3**
cur.execute("SELECT COUNT(*) FROM albums JOIN artists ON albums.ArtistId = artists.ArtistId WHERE artists.Name = 'Miles Davis';")

7. The longest song in the database is **Occupation / Precipice**
cur.execute("SELECT Name, MAX(Milliseconds) FROM tracks;")

8. The title of the longest album in the db is **"Lost, Season". with a total length of 70,665,582 Milliseconds**
query = SELECT al.Title AS AlbumTitle,
SUM(t.Milliseconds) AS TotalLength
FROM albums al
JOIN tracks t ON al.AlbumId = t.AlbumId
GROUP BY al.AlbumId
ORDER BY TotalLength DESC
LIMIT 1

9. The most expensive invoice id is **ID 404 with price of $25/86 and the customer name is Helena Holy.**
query = 
SELECT i.InvoiceId, i.Total, c.FirstName || ' ' || c.LastName AS CustomerName 
FROM invoices i 
JOIN customers c ON i.CustomerId = c.CustomerId 
ORDER BY i.Total DESC
LIMIT 1

10. The customer who spend the most is **Helena Holy**, with a total of $49.62.
query = 
SELECT c.FirstName || ' ' || c.LastName AS CustomerName, SUM(i.Total) AS TotalSpent 
FROM customers c 
JOIN invoices i On c.CustomerId = i.CustomerId
GROUP BY c.CustomerId
ORDER BY TotalSpent DESC
LIMIT 1
