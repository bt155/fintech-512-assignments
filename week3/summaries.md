# Your paper and video summaries go here.

# Rubric:
* Write a short summary of the paper/video, as follows:
* First line: Paper Title, citation source
* Keywords: List keywords from paper, or select 4-8 most relevant terms from the paper
* First paragraph: describe the main points of the paper/video.
* Second paragraph: Present the paper/video's strengths and weaknesses from your point of view.


My function generate_kwic_index encapsulates the core logic of KWIC index generation, similar to the second decomposition approach that Parnas discusses, which focuses on information hiding and modularization based on design decisions. Each step of the KWIC process (filtering ignore words, formatting titles, and sorting entries) is incorporated within this single function, suggesting a level of encapsulation.

However, the overall structure of my code also resembles the first decomposition approach. The process is linear, with each major task (input handling, index generation, and output formatting) handled in sequence, compare to Parnas' first decomposition where each step in the process is a separate module. This is evident in the main function, which sequentially processes input, calls generate_kwic_index, and then handles output.

'Rediscovering Simplicity' chapter in '99 Bottles of OOP', Metz:
The reading focuses on the evolution of a programmer's approach to writing code. Initially, programmers write simple code, but as they gain experience and learn new skills, their code becomes more complex and abstract, anticipating future changes. The chapter also discusses the trade-offs in design decisions and the costs of complexity, emphasizing Object-Oriented Design (OOD). It presents the challenge of finding the right abstraction level in code, balancing understandability with flexibility. The chapter uses the "99 Bottles of Beer" problem to illustrate these concepts, comparing different code solutions and their effectiveness in reflecting the problem's domain.

"Modern Code Review" emphasizes the importance of code review in software development, there are many key points such as:
__1.Two Heads are Better Than One__ Collaboration in code review with domain experts or more experienced developers, is beneficial. 
__2.Efficiency of Code Reviews__ Properly executed code reviews are shown to be effective in identifying bugs faster than testing or other debugging methods. 
__3.Size Limitation for Code Reviews__ Recommends not reviewing more than 400 lines of code in one session to maintain effectiveness in identifying defects. 
__4.The Importance of Context__ Stresses reviewing code with an understanding of its broader context rather than in isolation to uncover more defects.

"SOLID Object-Oriented Design, Sandi Metz" talks about the principles of OO design, particulary foucsing on SOLID principle which are strategies for managing dependencies in applications to minimize entanglements. There are SOLID principles as a means to avoid these issues:
__1.Single Responsibility Principle(SRP)__ A class should have only one reason to change, meaning it should have only one job.
__2.Open/Closed Principle__ Software entitles(classes, modules, functions, etc...) should be open for extension but closed for modification.
__3.Liskov Substitution Principle__ Objects of a superclass shall be replaceable with objects of a subclass without affecting the correctness of the program.
Then Metz gives examples of applying these principles, illustrating how adherence to them can lead to software that is easier to maintain and extend. She uses a case study involving a job to download an dprocess data to show how initially intertwined responsibilities within a single class can be refactored into a cleaner, more moduler design.

__ABC metric and SOLC metric:__ My ABC Metric is <14, 9, 6>, the Magnitude is sqrt(14^2 + 9^2 + 6^2), which the ABC metric for my code is 17.69. The code is 43 and the % is 74.1

