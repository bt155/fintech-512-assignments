class Money(Expression):
    def __init__(self, amount, currency):
        self._amount = amount
        self._currency = currency

    def currency(self):
        return self._currency

    def times(self, multiplier):
        return Money(self._amount * multiplier, self._currency)

    def plus(self, addend):
        return Sum(self, addend)

    def reduce(self, bank, to_currency):
        rate = bank.rate(self._currency, to_currency)
        return Money(self._amount / rate, to_currency)

    def __eq__(self, other):
        if not isinstance(other, Money):
            return False
        return self._amount == other._amount and self.currency() == other.currency()

    def __str__(self):
        return f"{self._amount} {self._currency}"

    @staticmethod
    def dollar(amount):
        return Money(amount, "USD")

    @staticmethod
    def franc(amount):
        return Money(amount, "CHF")


bank = Bank()
bank.add_rate("CHF", "USD", 2)
result = bank.reduce(Money.franc(2), "USD")
assert result == Money.dollar(1), "Currency conversion is incorrect"

assert bank.rate("USD", "USD") == 1, "Identity rate should be 1"

print("All tests passed!")
