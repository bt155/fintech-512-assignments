# Re-implementing the updated Money class to include the static methods 'dollar' and 'franc'

class Money(Expression):
    def __init__(self, amount, currency):
        self._amount = amount
        self._currency = currency

    def currency(self):
        return self._currency

    def times(self, multiplier):
        return Money(self._amount * multiplier, self._currency)

    def plus(self, addend):
        return Sum(self, addend)

    def reduce(self, bank, to_currency):
        rate = bank.rate(self._currency, to_currency)
        return Money(self._amount / rate, to_currency)

    def __eq__(self, other):
        if not isinstance(other, Money):
            return False
        return self._amount == other._amount and self.currency() == other.currency()

    def __str__(self):
        return f"{self._amount} {self._currency}"

    @staticmethod
    def dollar(amount):
        return Money(amount, "USD")

    @staticmethod
    def franc(amount):
        return Money(amount, "CHF")


# Tests
# Test for Sum.plus with mixed currencies
bank = Bank()
bank.add_rate("CHF", "USD", 2)
five_bucks = Money.dollar(5)
ten_francs = Money.franc(10)
sum_expression = Sum(five_bucks, ten_francs).plus(five_bucks)
result = bank.reduce(sum_expression, "USD")
assert result == Money.dollar(
    15), "Sum.plus with mixed currencies is incorrect"

# Test for Sum.times with mixed currencies
sum_expression = Sum(five_bucks, ten_francs).times(2)
result = bank.reduce(sum_expression, "USD")
assert result == Money.dollar(
    20), "Sum.times with mixed currencies is incorrect"

print("All tests passed!")
