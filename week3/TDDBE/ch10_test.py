class Money:
    def __init__(self, amount, currency):
        self._amount = amount
        self._currency = currency

    def currency(self):
        return self._currency

    def times(self, multiplier):
        return Money(self._amount * multiplier, self._currency)

    def __eq__(self, other):
        if not isinstance(other, Money):
            return False
        return self._amount == other._amount and self.currency() == other.currency()

    def __str__(self):
        return f"{self._amount} {self._currency}"


money1 = Money(5, "USD")
money2 = Money(10, "CHF")
money3 = money1.times(2)

print(money1)
print(money2)
print(money3)
