# FinTech 512 GitLab Weekly Assignments

This project provides the shell for the weekly assignments as well as external code used in the class.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
