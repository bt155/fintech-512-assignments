class Expression:
    def reduce(self, bank, to_currency):
        pass


class Money(Expression):
    def __init__(self, amount, currency):
        self._amount = amount
        self._currency = currency

    def currency(self):
        return self._currency

    def times(self, multiplier):
        return Money(self._amount * multiplier, self._currency)

    def plus(self, addend):
        return Money(self._amount + addend._amount, self._currency)

    def reduce(self, bank, to_currency):
        if self._currency == to_currency:
            return self
        return None

    def __eq__(self, other):
        if not isinstance(other, Money):
            return False
        return self._amount == other._amount and self.currency() == other.currency()

    def __str__(self):
        return f"{self._amount} {self._currency}"

    @staticmethod
    def dollar(amount):
        return Money(amount, "USD")

    @staticmethod
    def franc(amount):
        return Money(amount, "CHF")


class Bank:
    def reduce(self, source, to_currency):
        return source.reduce(self, to_currency)


five = Money.dollar(5)
sum_expression = five.plus(five)
bank = Bank()
reduced = bank.reduce(sum_expression, "USD")

assert reduced == Money.dollar(10), "Reduced sum is incorrect"

print("Test passed: 5 USD + 5 USD = 10 USD")
