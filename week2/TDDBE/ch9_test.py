class Money:
    def __init__(self, amount, currency):
        self._amount = amount
        self._currency = currency

    def currency(self):
        return self._currency

    @staticmethod
    def dollar(amount):
        return Dollar(amount, "USD")

    @staticmethod
    def franc(amount):
        return Franc(amount, "CHF")

    def times(self, multiplier):
        raise NotImplementedError


class Franc(Money):
    def __init__(self, amount, currency="CHF"):
        super().__init__(amount, currency)

    def times(self, multiplier):
        return Money.franc(self._amount * multiplier)


class Dollar(Money):
    def __init__(self, amount, currency="USD"):
        super().__init__(amount, currency)

    def times(self, multiplier):
        return Money.dollar(self._amount * multiplier)


dollar = Money.dollar(1)
franc = Money.franc(1)

(dollar.currency(), franc.currency())
