class Expression:
    def reduce(self, bank, to_currency):
        pass


class Sum(Expression):
    def __init__(self, augend, addend):
        self.augend = augend
        self.addend = addend

    def reduce(self, bank, to_currency):
        amount = self.augend.reduce(
            bank, to_currency)._amount + self.addend.reduce(bank, to_currency)._amount
        return Money(amount, to_currency)


class Money(Expression):
    def __init__(self, amount, currency):
        self._amount = amount
        self._currency = currency

    def currency(self):
        return self._currency

    def times(self, multiplier):
        return Money(self._amount * multiplier, self._currency)

    def plus(self, addend):
        return Sum(self, addend)

    def reduce(self, bank, to_currency):
        if self._currency == to_currency:
            return self
        return None

    def __eq__(self, other):
        if not isinstance(other, Money):
            return False
        return self._amount == other._amount and self.currency() == other.currency()

    def __str__(self):
        return f"{self._amount} {self._currency}"

    @staticmethod
    def dollar(amount):
        return Money(amount, "USD")

    @staticmethod
    def franc(amount):
        return Money(amount, "CHF")


class Bank:
    def reduce(self, source, to_currency):
        return source.reduce(self, to_currency)


five = Money.dollar(5)
sum_expression = five.plus(five)
assert isinstance(sum_expression, Sum), "Result of plus should be a Sum"
assert sum_expression.augend == five, "Augend is incorrect"
assert sum_expression.addend == five, "Addend is incorrect"

sum_expression = Sum(Money.dollar(3), Money.dollar(4))
bank = Bank()
result = bank.reduce(sum_expression, "USD")
assert result == Money.dollar(7), "Reduced sum is incorrect"

result = bank.reduce(Money.dollar(1), "USD")
assert result == Money.dollar(1), "Reduced money is incorrect"

print("All tests passed!")
