Requirement #: user-01	
Requirement Type: Function requirements
Event/Use Case #:N/A
Description: Enable Customers to interact and create accounts
Ratonale: Ability for customers to create and interact with their accounts is fundamental to any online trading platform.
Originator: user-01
Fit Cirterion: We can track the number of account created, as well as sending out survey to users
Customer Satisfaction: 5 
Customer Dissatisfaction: 0
Conflicts: N/A
Priority: 10/10
Supporting Materials: N/A
History: N/A

Requirement #: user-02
Requirement Type: Function requirements
Event/Use Case #:N/A
Description: Enable Customers to buy and sell shares of stock
Ratonale: Implement functionality for account holders to buy and sell shares, trackign them in their accounts. This feature needs to be robust and reliable, as High Net Worth individuals are likely to execute large transactions. 
Originator: user-02
Fit Cirterion: Implement test cases for both 'buy' and 'sell' functionalities using historical data. Measure the accuracy of these transactions against expected outcomes.
Customer Satisfaction: 5 
Customer Dissatisfaction: 0
Conflicts: N/A
Priority: 10/10
Supporting Materials: N/A
History: N/A

Requirement #: user-03
Requirement Type: Function requirements
Event/Use Case #:N/A
Description: Enable Customers to Consider History in Evaluating Holdings
Ratonale: Access to historical data is crucial for making informed investment decisions. It allows users to analyze trends, evaluate the performance of their holdings over time, and make predictions about future performance. 
Originator: user-03
Fit Cirterion: Verify the database contains at least five years of historical data for each stock and the SPY index. This can be measured by checking the records' timestamps. 
Customer Satisfaction: 5 
Customer Dissatisfaction: 0
Conflicts: N/A
Priority: 10/10
Supporting Materials: N/A
History: N/A

Requirement #: user-04
Requirement Type: Function requirements
Event/Use Case #:N/A
Description: Enable Customers to Analyze the Risk-Return Profile of Their Portfolio
Ratonale: High Net Worth individuals often have diversified portfolios and need sophisticated tools to analyze risk and return.
Originator: user-04
Fit Cirterion: Test the implementation of the efficient frontier and Sharpe ratio calculations by comparing the output with known benchmarks or theoretical expectations.
Customer Satisfaction: 5 
Customer Dissatisfaction: 0
Conflicts: N/A
Priority: 8/10
Supporting Materials: N/A
History: N/A

Requirement #: user-05
Requirement Type: Function requirements
Event/Use Case #:N/A
Description: Enable Customers to Run Reports on Their Holdings
Ratonale: Reporting features enable users to easily view and understand their portfolio's current status. 
Originator: user-04
Fit Cirterion: Validate the reporting functionality by ensuring reports accurately reflect the current holdings, prices, and transactions.
Customer Satisfaction: 5 
Customer Dissatisfaction: 0
Conflicts: N/A
Priority: 7/10
Supporting Materials: N/A
History: N/A