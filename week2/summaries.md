# Your paper and video summaries go here.

# Rubric:
* Write a short summary of the paper/video, as follows:
* First line: Paper Title, citation source
* Keywords: List keywords from paper, or select 4-8 most relevant terms from the paper
* First paragraph: describe the main points of the paper/video.
* Second paragraph: Present the paper/video's strengths and weaknesses from your point of view.

# 'What Are Requirements' chapter in Mastering the Requirements Process
This Chapter focus on the importance of understanding requirements in software development as well as project management. the success of any product is deeply rooted in how well its requirements are understood, irrespective of the development methodology employed. There are numbers of key aspects include:
**1. Define the Requirements :** Requirements are a statement of business needs, without any implementation bias. Or in another word, what the product must do or prossess to be 
useful. Understand the actual requirement is crucial for the success
**2. Process of Requirements :** The requirements process is a thorough exploration of the intended product with the intention of discovering, leading to a written descriptio nof its functionality and behavior. 
**3. Types of Requirements :** For the types of requirements, it categorizes requirements into functional, nonfunctional, and constraints
**4. Evolution of Requirements :** The evolution of the product and its requirements is a process that we cannot control or prescribe, but it is one that we must accept. So bascially what the author means is that requirementse evolve from vague ideas to precise, testable specifications throughtout the development process.

Weaknesses and Strengths:
Strengths:
**Comprehensive Apporach :** The chapter offers a detailed and thorough appraoch to understanding and managin requirements, covering various aspects crucial for project success.
**Methodology Neutral :** The methodology can ensure the focus remain on business needs rather than getting constrained by specific technologies or methodologies.
**Clear Categorization :** Requirement's categorization into functional, nonfunctinal, and constraints provides clarity and structure in requirement analysis.

Weaknesses:
**Potential Complexity :** The depth and comprehensiveness of the approach might be overwhelming and could lead to implementation challenges.
**Technology Neutrality Risks :** Technologically neutral is generally beneficial, it might sometimes delay the consideration of important technical constraints and real-world feasibility.
**Adaptation :** Requirements usually change rapidly, the structured apporach might need to be more adaptable or responsive to these changes.   


# A Laboratory For Teaching Object-Oriented Thinking
The article by Kent Beck and Ward Cunningham present an innovative approach to bridge the gap between procedural and OO programming for learner at various levels. The cornerstone of the methodology is the use of CRC cards, a tool designed to go though the OO concepts. And this method not only assists beginners in understanding the fundamental OO concepts but also help them experienced programmers in appreciating the designs. 

Weaknesses and Strengths:
Strengths:
**Innovative Apporach :** The intro of CRC(Class-Responsibility-Collaborator) cards is a highly innovative approach to teach object-oriented programming. This method provides a tangible, interactive way for learners to understand and apply oo concepts, which is a significant departure fro mmroe traditional, theory-heavy teaching methods.
**Practicality and Accessibility :** The CRC cards are not only easy to understand but also practical. It make abstract concepts of oo programing more accessible, especially for those transitioning from procedual programming.
**Flexibility :** CRC card method is flexible to various learning enviornments and contexts. This method can be tailored to suit different learning needs and scenarios.

Weaknesses:
**Limited Scope :** Article primarily focus on the CRC card method where it overlooking otehr teaching strategies for oo programming. This narrow focus limited others
**Prior Programming Knowledge :** Assue learner to have prior background in procedural programming. This assumption could limit its applicability for absolute beginners who might benefit from a more foundatianl intro to programming concepts before diving into oo principles.

# HTML Fundamentals
This chapter give a fundamental of the HTML struture, syntax of HTML, including tags to denote elements like headings, paragraphs, hyperlinks and images. For tag like <h1>, <h2> describe the different levels of heading, and tags are case-insensitive and all of them should be written in lowercase. Comments in HTML are made using <!-- and -->, while the attributes in start tags provide additional information like <a href="url"> for hyperlinks. For the HTML basic template, it should includes <!DOCTYPE html>, <html>, <head>, and <body> tags.