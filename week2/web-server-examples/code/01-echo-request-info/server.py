import http.server
import random

# -------------------------------------------------------------------------------


class RequestHandler(http.server.BaseHTTPRequestHandler):
    Intro = [
        "Hi This is Benjamin",
        "I'm currently a Master student of FinTech at Duke University!",
        "This is Random Contenct 1",
        "This is Random Contenct 2",
        "This is Random Contenct 3",
        "This is Random Contenct 4",
        "This is Random Contenct 5",
    ]

    # Template for page to send back.
    Page = '''\
<html>
<body>
    <h1> Self Intro </h1>
    <p>{Intro}</p>
</body>
</html>
'''

    # Handle a request by constructing an HTML page that echoes the
    # request back to the caller.
    def do_GET(self):
        page = self.create_page()
        self.send_page(page)

    # Create an information page to send.
    def create_page(self):
        Intro = random.choice(self.Intro)
        page = self.Page.format(Intro=Intro)
        return page

    # Send the created page.
    def send_page(self, page):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.send_header("Content-Length", str(len(page)))
        self.end_headers()
        self.wfile.write(page.encode('utf-8'))

# -------------------------------------------------------------------------------


if __name__ == '__main__':
    serverAddress = ('', 3000)
    server = http.server.HTTPServer(serverAddress, RequestHandler)
    server.serve_forever()
